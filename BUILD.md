# Build Instructions

## Windows
You'll need Visual Studio 2017, and you'll find the solution under "projects/vs2017"

## Linux
You'll need C++17 compiler, and you'll find the make files under "projects/gmake"